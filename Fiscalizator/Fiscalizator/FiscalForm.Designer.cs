﻿namespace Fiscalizator
{
    partial class FiscalForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FiscalForm));
            this.button_add = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.button_exit = new System.Windows.Forms.Button();
            this.button_sale = new System.Windows.Forms.Button();
            this.label_Item = new System.Windows.Forms.Label();
            this.textBox_InputItem = new System.Windows.Forms.TextBox();
            this.textBox_price = new System.Windows.Forms.TextBox();
            this.label_Price = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDown_Quantity = new System.Windows.Forms.NumericUpDown();
            this.Quantity = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.comboBox_tax = new System.Windows.Forms.ComboBox();
            this.comboBox_TypePayment = new System.Windows.Forms.ComboBox();
            this.label_summ = new System.Windows.Forms.Label();
            this.textBox_summ = new System.Windows.Forms.TextBox();
            this.comboBox_SaleOrReturn = new System.Windows.Forms.ComboBox();
            this.LabelMessage = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Quantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(458, 84);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(148, 32);
            this.button_add.TabIndex = 5;
            this.button_add.Text = "Добавить позицию";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(621, 242);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(149, 36);
            this.Delete.TabIndex = 10;
            this.Delete.Text = "Удалить позицию";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.button_Delete);
            // 
            // button_exit
            // 
            this.button_exit.Location = new System.Drawing.Point(621, 284);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(149, 36);
            this.button_exit.TabIndex = 11;
            this.button_exit.Text = "Выход";
            this.button_exit.UseVisualStyleBackColor = true;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // button_sale
            // 
            this.button_sale.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_sale.Location = new System.Drawing.Point(621, 137);
            this.button_sale.Name = "button_sale";
            this.button_sale.Size = new System.Drawing.Size(149, 36);
            this.button_sale.TabIndex = 9;
            this.button_sale.Text = "Продажа";
            this.button_sale.UseVisualStyleBackColor = true;
            this.button_sale.Click += new System.EventHandler(this.button_Sale_Click);
            // 
            // label_Item
            // 
            this.label_Item.AutoSize = true;
            this.label_Item.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Item.Location = new System.Drawing.Point(13, 410);
            this.label_Item.Name = "label_Item";
            this.label_Item.Size = new System.Drawing.Size(83, 24);
            this.label_Item.TabIndex = 6;
            this.label_Item.Text = "Артикул";
            // 
            // textBox_InputItem
            // 
            this.textBox_InputItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_InputItem.Location = new System.Drawing.Point(150, 406);
            this.textBox_InputItem.Name = "textBox_InputItem";
            this.textBox_InputItem.Size = new System.Drawing.Size(465, 32);
            this.textBox_InputItem.TabIndex = 1;
            this.textBox_InputItem.TextChanged += new System.EventHandler(this.textBox_InputItem_TextChanged);
            // 
            // textBox_price
            // 
            this.textBox_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_price.Location = new System.Drawing.Point(141, 88);
            this.textBox_price.Name = "textBox_price";
            this.textBox_price.Size = new System.Drawing.Size(128, 32);
            this.textBox_price.TabIndex = 4;
            this.textBox_price.TextChanged += new System.EventHandler(this.textBox_Price_TextChanged);
            this.textBox_price.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_price_KeyPress);
            // 
            // label_Price
            // 
            this.label_Price.AutoSize = true;
            this.label_Price.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Price.Location = new System.Drawing.Point(3, 92);
            this.label_Price.Name = "label_Price";
            this.label_Price.Size = new System.Drawing.Size(54, 24);
            this.label_Price.TabIndex = 6;
            this.label_Price.Text = "Цена";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDown_Quantity);
            this.groupBox1.Controls.Add(this.textBox_price);
            this.groupBox1.Controls.Add(this.Quantity);
            this.groupBox1.Controls.Add(this.label_Price);
            this.groupBox1.Controls.Add(this.button_add);
            this.groupBox1.Location = new System.Drawing.Point(9, 392);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(616, 138);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // numericUpDown_Quantity
            // 
            this.numericUpDown_Quantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDown_Quantity.Location = new System.Drawing.Point(141, 52);
            this.numericUpDown_Quantity.Name = "numericUpDown_Quantity";
            this.numericUpDown_Quantity.Size = new System.Drawing.Size(128, 32);
            this.numericUpDown_Quantity.TabIndex = 7;
            this.numericUpDown_Quantity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Quantity
            // 
            this.Quantity.AutoSize = true;
            this.Quantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Quantity.Location = new System.Drawing.Point(3, 56);
            this.Quantity.Name = "Quantity";
            this.Quantity.Size = new System.Drawing.Size(71, 24);
            this.Quantity.TabIndex = 6;
            this.Quantity.Text = "Кол-во";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Location = new System.Drawing.Point(17, 12);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Size = new System.Drawing.Size(598, 308);
            this.dataGridView1.TabIndex = 11;
            this.dataGridView1.TabStop = false;
            // 
            // comboBox_tax
            // 
            this.comboBox_tax.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBox_tax.FormattingEnabled = true;
            this.comboBox_tax.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.comboBox_tax.IntegralHeight = false;
            this.comboBox_tax.Items.AddRange(new object[] {
            "НДС 20%",
            "НДС 10%"});
            this.comboBox_tax.Location = new System.Drawing.Point(621, 12);
            this.comboBox_tax.Name = "comboBox_tax";
            this.comboBox_tax.Size = new System.Drawing.Size(149, 21);
            this.comboBox_tax.TabIndex = 6;
            this.comboBox_tax.Text = "Налоговая ставка";
            // 
            // comboBox_TypePayment
            // 
            this.comboBox_TypePayment.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBox_TypePayment.FormattingEnabled = true;
            this.comboBox_TypePayment.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.comboBox_TypePayment.IntegralHeight = false;
            this.comboBox_TypePayment.Items.AddRange(new object[] {
            "Наличные",
            "Безналичные"});
            this.comboBox_TypePayment.Location = new System.Drawing.Point(621, 39);
            this.comboBox_TypePayment.Name = "comboBox_TypePayment";
            this.comboBox_TypePayment.Size = new System.Drawing.Size(149, 21);
            this.comboBox_TypePayment.TabIndex = 7;
            this.comboBox_TypePayment.Text = "Вид оплаты";
            // 
            // label_summ
            // 
            this.label_summ.AutoSize = true;
            this.label_summ.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_summ.Location = new System.Drawing.Point(398, 327);
            this.label_summ.Name = "label_summ";
            this.label_summ.Size = new System.Drawing.Size(68, 24);
            this.label_summ.TabIndex = 6;
            this.label_summ.Text = "Сумма";
            // 
            // textBox_summ
            // 
            this.textBox_summ.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_summ.Location = new System.Drawing.Point(487, 323);
            this.textBox_summ.Name = "textBox_summ";
            this.textBox_summ.ReadOnly = true;
            this.textBox_summ.Size = new System.Drawing.Size(128, 32);
            this.textBox_summ.TabIndex = 12;
            this.textBox_summ.TabStop = false;
            this.textBox_summ.Text = "0";
            this.textBox_summ.TextChanged += new System.EventHandler(this.textBox_Price_TextChanged);
            // 
            // comboBox_SaleOrReturn
            // 
            this.comboBox_SaleOrReturn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBox_SaleOrReturn.FormattingEnabled = true;
            this.comboBox_SaleOrReturn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.comboBox_SaleOrReturn.IntegralHeight = false;
            this.comboBox_SaleOrReturn.Items.AddRange(new object[] {
            "Продажа",
            "Возврат"});
            this.comboBox_SaleOrReturn.Location = new System.Drawing.Point(621, 66);
            this.comboBox_SaleOrReturn.Name = "comboBox_SaleOrReturn";
            this.comboBox_SaleOrReturn.Size = new System.Drawing.Size(149, 21);
            this.comboBox_SaleOrReturn.TabIndex = 8;
            this.comboBox_SaleOrReturn.Text = "Продажа/возврат";
            // 
            // LabelMessage
            // 
            this.LabelMessage.AutoSize = true;
            this.LabelMessage.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LabelMessage.ForeColor = System.Drawing.Color.Red;
            this.LabelMessage.Location = new System.Drawing.Point(17, 355);
            this.LabelMessage.Name = "LabelMessage";
            this.LabelMessage.Size = new System.Drawing.Size(76, 13);
            this.LabelMessage.TabIndex = 13;
            this.LabelMessage.Text = "LabelMessage";
            this.LabelMessage.Click += new System.EventHandler(this.LabelMessage_Click);
            // 
            // FiscalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(782, 542);
            this.Controls.Add(this.LabelMessage);
            this.Controls.Add(this.textBox_summ);
            this.Controls.Add(this.comboBox_SaleOrReturn);
            this.Controls.Add(this.comboBox_TypePayment);
            this.Controls.Add(this.comboBox_tax);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.textBox_InputItem);
            this.Controls.Add(this.button_sale);
            this.Controls.Add(this.button_exit);
            this.Controls.Add(this.label_summ);
            this.Controls.Add(this.label_Item);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(798, 581);
            this.MinimumSize = new System.Drawing.Size(798, 581);
            this.Name = "FiscalForm";
            this.Text = "Fiscalizator";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Quantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.Button button_sale;
        private System.Windows.Forms.TextBox textBox_InputItem;
        private System.Windows.Forms.Label label_Item;
        private System.Windows.Forms.TextBox textBox_price;
        private System.Windows.Forms.Label label_Price;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBox_tax;
        private System.Windows.Forms.ComboBox comboBox_TypePayment;
        private System.Windows.Forms.Label Quantity;
        private System.Windows.Forms.Label label_summ;
        private System.Windows.Forms.TextBox textBox_summ;
        private System.Windows.Forms.ComboBox comboBox_SaleOrReturn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.NumericUpDown numericUpDown_Quantity;
        protected System.Windows.Forms.Label LabelMessage;
    }
}

