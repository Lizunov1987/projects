﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Fiscalizator
{
    
    class FiscalViewModel : INotifyPropertyChanged
    {
        Fiscal fiscal = new Fiscal();
        public BindingList<Item> Receipt { get; set; } = new BindingList<Item>();
        private string _TextBoxItem = "";
        private int _TextBoxQuantity = 1;
        private decimal _TextBoxPrice = 0;
        private decimal _TextBoxSum = 0;
        private int _ComboBoxTaxSelectedIndex = -1;
        private int _ComboBoxTypePaymentSelectedIndex = -1;
        private int _ComboBoxSaleOrReturnSelectedIndex = -1;
        private string _LabelMessage ="";
        
        public string TextBoxItem
        {
            get { return _TextBoxItem; }
            set
            {
                _TextBoxItem = value;
                OnPropertyChanged();
            }
        }
        public decimal TextBoxPrice
        {
            get { return _TextBoxPrice; }
            set
            {
                _TextBoxPrice = value;
                OnPropertyChanged();
            }
        }
        public int TextBoxQuantity
        {
            get { return _TextBoxQuantity; }
            set
            {
                _TextBoxQuantity = value;
                OnPropertyChanged();
            }
        }
        public decimal TextBoxSum
        {
            get { return _TextBoxSum; }
            set
            {
                _TextBoxSum = value;
                OnPropertyChanged();
            }
        }
        public int ComboBoxTaxSelectedIndex
        {
            get { return _ComboBoxTaxSelectedIndex; }
            set
            {
                _ComboBoxTaxSelectedIndex = value;
                OnPropertyChanged();
            }
        }
        public int ComboBoxTypePaymentSelectedIndex
        {
            get { return _ComboBoxTypePaymentSelectedIndex; }
            set
            {
                _ComboBoxTypePaymentSelectedIndex = value;
                OnPropertyChanged();
            }
        }
        public int ComboBoxSaleOrReturnSelectedIndex
        {
            get { return _ComboBoxSaleOrReturnSelectedIndex; }
            set
            {
                _ComboBoxSaleOrReturnSelectedIndex = value;
                OnPropertyChanged();
            }
        }
        public string LabelMessage
        {
            get { return _LabelMessage; }
            set
            {
                _LabelMessage = value;
                OnPropertyChanged();
            }
        }
        public bool CanAdd
        {
            get
            {
                if (string.IsNullOrEmpty(TextBoxItem))
                {
                    return false;
                }
                else if (TextBoxQuantity == 0)
                {
                    return false;
                }
                return true;
            }
        }
        public bool CanSale
        {
            get
            {
                if (ComboBoxTaxSelectedIndex < 0)
                {
                    return false;
                }
                else if (ComboBoxSaleOrReturnSelectedIndex<0)
                {
                    return false;
                }
                else if (ComboBoxTypePaymentSelectedIndex < 0)
                {
                    return false;
                }
                else if (Receipt.Count < 1)
                {
                    return false;
                }
                return true;
            }
        }
        public bool IsConnectedToFR
        { get; private set; }

        public void AddItemToReceipt()
        {
            Receipt.Add(new Item() { Name = TextBoxItem, Quantity = TextBoxQuantity, Price = TextBoxPrice, Cost = (TextBoxQuantity * TextBoxPrice) });
            TextBoxSum += (TextBoxPrice * TextBoxQuantity);
            TextBoxItem = "";
            TextBoxPrice = 0;
            TextBoxQuantity = 1;
        }
        public void Sale()
        {
            try
            {
                TypePaymnt _typePayment = ComboBoxTypePaymentSelectedIndex == 0 ? TypePaymnt.Cash : TypePaymnt.CreditCard;
                CheckType _saleOrReturn = ComboBoxSaleOrReturnSelectedIndex == 0 ? CheckType.Sale : CheckType.Return;
                Tax _tax = ComboBoxTaxSelectedIndex == 0 ? Tax.VAT18 : Tax.VAT10;
                fiscal.ProcessPayment(_typePayment, _saleOrReturn, _tax, Receipt); //typePayment(0-cash/1-credit), sale or return, tax, List<Item> Receipt
            }
            catch (Exception ex)
            {
                LabelMessage = ex.Message;
            }
            finally
            {
                Receipt.Clear();
                OnPropertyChanged("CanSale");
                _SummCalculate();
            }
        }
        public void connectToFR()
        {
            try
            {
                fiscal.Connect();
            }
            catch (Exception ex)
            {
                IsConnectedToFR = false;
                LabelMessage = ex.Message;
            }
        }
        public void disconnectFR()
        {
            fiscal.Disconnect();
        }
        public void DeleteItem(int index)
        {
            Receipt.RemoveAt(index);
            _SummCalculate();
        }
        void _SummCalculate()
        {
            TextBoxSum = 0;
            foreach (Item item in Receipt)
            {
                TextBoxSum += item.Cost;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
