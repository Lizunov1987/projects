﻿namespace Fiscalizator
{
    public enum Tax { WithoutVAT = 0, VAT18, VAT10, VAT0, WithoutVAT2, VAT18_118, VAT10_110 }
}
