﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Fiscalizator
{
    class Fiscal
    {
        public bool IsConnected { get; private set; }
        private DrvFRLib.DrvFR Drv = null;

        private const int adminPassword = 30;

        public void ProcessPayment(TypePaymnt typePayment, CheckType сheckType, Tax tax, IEnumerable<Item> Receipt)
        {
            foreach (Item item in Receipt)
            {
                const int sectionNumber = 1; //from API, page 110
                Drv.Password = adminPassword;
                Drv.CheckType = (int)сheckType;

                Drv.Quantity = item.Quantity; 
                Drv.Price = item.Price;
                Drv.Summ1Enabled = true;
                summToZero();

                Drv.TaxValueEnabled = false;
                Drv.Summ1 = item.Cost;
                Drv.Tax1 = (int)tax;
                Drv.Department = sectionNumber;
                const int _typePaymentFullPay = 4;
                Drv.PaymentTypeSign = _typePaymentFullPay;
                const int paymentItemSignProduct = 1;
                Drv.PaymentItemSign = paymentItemSignProduct;
                Drv.StringForPrinting = item.Name;
                
                if (Drv.FNOperation() != 0)
                {
                    int errorCode = Drv.ErrorCode;
                    string error = Drv.ResultCodeDescription;
                    Drv.SysAdminCancelCheck();
                    throw new Exception($"Не удалось добавить \"{item.Name}\" в чек. Ошибка при обращении к ФР\nПричина: \"({errorCode}) {error}\"");
                }
            }
            decimal summ = Receipt.Sum(x => x.Cost);
            summToZero();

            if (typePayment == TypePaymnt.Cash)
            {
                Drv.Summ1 = summ;
            }
            else if (typePayment == TypePaymnt.CreditCard)
            {
                Drv.Summ4 = summ;
            }

            Drv.RoundingSumm = 0;
            Drv.TaxValue1 = 0M;
            Drv.TaxValue2 = 0M;
            Drv.TaxValue3 = 0M;
            Drv.TaxValue4 = 0M;
            Drv.TaxValue5 = 0M;
            Drv.TaxValue6 = 0M;
            const int _taxTypeMain = 0b000001;
            Drv.TaxType = _taxTypeMain;
            Drv.StringForPrinting = "================================================";
            if (Drv.FNCloseCheckEx() != 0)
            {
                int errorCode = Drv.ErrorCode;
                string error = Drv.ResultCodeDescription;
                Drv.SysAdminCancelCheck();
                throw new Exception($"Не удалось закрыть чек, продажа отменена\nПричина: \"({errorCode}) {error}\"");
            }
            Disconnect();
        }
        private void summToZero()
            {
                Drv.Summ1 = 0M;
                Drv.Summ2 = 0M;
                Drv.Summ3 = 0M;
                Drv.Summ4 = 0M;
                Drv.Summ5 = 0M;
                Drv.Summ6 = 0M;
                Drv.Summ7 = 0M;
                Drv.Summ8 = 0M;
                Drv.Summ9 = 0M;
                Drv.Summ10 = 0M;
                Drv.Summ11 = 0M;
                Drv.Summ12 = 0M;
                Drv.Summ13 = 0M;
                Drv.Summ14 = 0M;
                Drv.Summ15 = 0M;
                Drv.Summ16 = 0M;
            }

        enum BaudRate { _2400, _4800, _9600, _192000, _38400, _57600, _115200 }
        enum ConnectionType { locally = 0, KKMServerTCP, KKMserverDCOM, escape, notUsed, emulator, TCPSocket }
        public void Connect()
        {
            Drv = new DrvFRLib.DrvFR
            {
                ComNumber = 1,
                BaudRate = (int)BaudRate._115200,
                Password = adminPassword
            };
            Drv.ConnectionType = (int)ConnectionType.locally;
            Drv.Connect();
            IsConnected = true;
        }
        public void Disconnect()
        {
            Drv.Disconnect();
            IsConnected = false;
        }
    }

    public class Item
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Cost { get; set; }
    }
}
