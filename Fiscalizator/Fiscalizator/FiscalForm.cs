﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace Fiscalizator
{
    public partial class FiscalForm : Form
    {
        FiscalViewModel ViewModel = new FiscalViewModel();
        BindingSource mainFormSourceViewModel;

        public FiscalForm()
        {

            InitializeComponent();

            ViewModel.connectToFR();
            if (ViewModel.IsConnectedToFR)
            {
                LabelMessage.Text = "Не удалось подключиться к фискальному регистратору";
                //LabelMessage.ForeColor = Color.Red;
                //LabelMessage.Visible = true;
                textBox_InputItem.ReadOnly = true;
                textBox_price.ReadOnly = true;
                numericUpDown_Quantity.ReadOnly = true;
            }
            this.mainFormSourceViewModel = new System.Windows.Forms.BindingSource(this.components);
            mainFormSourceViewModel.DataSource = ViewModel;
            LabelMessage.DataBindings.Add("Text", ViewModel, "LabelMessage", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox_InputItem.DataBindings.Add("Text", ViewModel, "TextBoxItem", true, DataSourceUpdateMode.OnPropertyChanged);
            numericUpDown_Quantity.DataBindings.Add("Text", ViewModel, "TextBoxQuantity", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox_price.DataBindings.Add("Text", ViewModel, "TextBoxPrice", true, DataSourceUpdateMode.OnPropertyChanged);
            textBox_summ.DataBindings.Add("Text", ViewModel, "TextBoxSum", true, DataSourceUpdateMode.OnPropertyChanged);
            button_add.DataBindings.Add("Enabled", ViewModel, "canAdd", true, DataSourceUpdateMode.OnPropertyChanged);
            button_sale.DataBindings.Add("Enabled", ViewModel, "canSale", true, DataSourceUpdateMode.OnPropertyChanged);
            comboBox_tax.DataBindings.Add("SelectedIndex", ViewModel, "ComboBoxTaxSelectedIndex", true, DataSourceUpdateMode.OnPropertyChanged);
            comboBox_TypePayment.DataBindings.Add("SelectedIndex", ViewModel, "ComboBoxTypePaymentSelectedIndex", true, DataSourceUpdateMode.OnPropertyChanged);
            comboBox_SaleOrReturn.DataBindings.Add("SelectedIndex", ViewModel, "ComboBoxSaleOrReturnSelectedIndex", true, DataSourceUpdateMode.OnPropertyChanged);

            dataGridView1.DataSource = ViewModel.Receipt;

        }
        private void button_Sale_Click(object sender, EventArgs e)
        {
            ViewModel.Sale();
        }
        private void button_Delete(object sender, EventArgs e) 
        {
            ViewModel.DeleteItem(dataGridView1.SelectedCells[0].RowIndex);
        }
        
        private void button_add_Click(object sender, EventArgs e)
        {
            ViewModel.AddItemToReceipt();
            textBox_InputItem.Focus();
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            if (ViewModel.IsConnectedToFR)
            {
                ViewModel.disconnectFR();
            }
            this.Close();
        }

        private void textBox_InputItem_TextChanged(object sender, EventArgs e)
        {
            this.AcceptButton = button_add;
        }

        private void textBox_Price_TextChanged(object sender, EventArgs e)
        {
            this.AcceptButton = button_add;
        }

        private void TextBox_price_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ',') )
            {
                e.Handled = true;
            }
        }

        private void LabelMessage_Click(object sender, EventArgs e)
        {
            LabelMessage.Text = "";
        }
    }

}
